<?php

namespace App\Controller;

use App\Entity\Demo;
use App\Form\DemoType;
use App\Repository\DemoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use \DateTime;

/**
 * @Route("/api")
 */
class ApiController extends AbstractController
{
    /**
     * @Route("/", name="api_index", methods={"GET"})
     */
    public function index(DemoRepository $demoRepository): Response
    {
        $data = $demoRepository->findAll();
        return $this->json($data);
    }

    /**
     * @Route("/new", name="api_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $data = json_decode($request->getContent());
        dump($request->getContent());
        dump($data);

        $demo = new Demo();
        dump($demo);

        $d = new DateTime($data->createdAt);
        dump($d);

        $demo->setName($data->name);
        $demo->setCreatedAt($d);
        $demo->setScore($data->score);
        dump($demo);
        
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($demo);
        $entityManager->flush();
        dump($demo);
        return $this->json($demo);
    }
}
